using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using SimpleInjector;
using Xamarin.Forms;
using Xunit;
using ZoransMagicBox.Tests.Annotations;
using Container = SimpleInjector.Container;

namespace ZoransMagicBox.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var con = new Container();
            
            con.Register<ITestService, TestService>(Lifestyle.Singleton);
            
            Assert.NotNull(con.GetInstance<ITestService>());
        }
        
        [Fact]
        public void Test2()
        {
            var con = new Container();
            
            con.Register<ITestDependency1, TestDependency1>(Lifestyle.Singleton);
            con.Register<ITestService, TestService>(Lifestyle.Singleton);
            
            Assert.NotNull(con.GetInstance<ITestService>());
            Assert.NotNull(con.GetInstance<ITestService>().TestDependency1);
        }
        
        [Fact]
        public void Test3()
        {
            var con = new Container();
            
            con.Register<ITestElement1, TestElement1>(Lifestyle.Transient);
            con.Register<ITestDependency1, TestDependency1>(Lifestyle.Singleton);
            con.Register<ITestService, TestService>(Lifestyle.Singleton);

            var testElem1 = con.GetInstance<ITestElement1>();

            Assert.NotNull(testElem1);

            testElem1.BindingContext = new TestBindingContext();
            
            Assert.NotNull(testElem1.TestDependency1);
            Assert.NotNull(testElem1.TestService);
            Assert.NotNull(testElem1.BindingContext);
        }
    }
    
    public interface ITestDependency1
    {
        
    }
    
    public class TestDependency1 : ITestDependency1
    {
        
    }

    public interface ITestService
    {
        ITestDependency1 TestDependency1 { get; }
    }

    public class TestService : ITestService
    {
        public ITestDependency1 TestDependency1 { get; }

        public TestService(ITestDependency1 testDependency1)
        {
            TestDependency1 = testDependency1;
        }
    }
    
    public interface IOnInit
    {
        Task OnInit();
    }
    
    public interface IOnInitAsync
    {
        Task OnInitAsync();
    }
    
    public class TestBindingContext : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public interface ITestElement
    {
        object BindingContext { get; set; }
    }

    public interface ITestElement1 : ITestElement
    {
        ITestDependency1 TestDependency1 { get; }
        ITestService TestService { get; }
    }
    
    public class TestElement1 : View, ITestElement1
    {
        public ITestDependency1 TestDependency1 { get; }
        public ITestService TestService { get; }

        public TestElement1(ITestDependency1 testDependency1, ITestService testService)
        {
            TestDependency1 = testDependency1;
            TestService = testService;
        }
    }
}