﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZoransMagicBox.Core.DI;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            var di = App.Container.GetInstance<IElementContainer>();

            var stackLayout = di.GetInstance<IStackElement>();
            stackLayout.Orientation = Orientation.Horizontal;
            
            var stackLayout2 = di.GetInstance<IStackElement>();
            stackLayout2.Orientation = Orientation.Vertical;
            stackLayout2.Width = 200;
            
            var stackLayout3 = di.GetInstance<IStackElement>();
            stackLayout3.Orientation = Orientation.Vertical;

            var label = di.GetInstance<ILabel>();
            label.Text = "\uf15b";
            label.FontFamily = "FontAwesome5FreeSolid";

            var label2 = di.GetInstance<ILabel>();
            label2.Text = "\uf15b";
            label2.FontFamily = "FontAwesome5BrandsRegular";

            var label3 = di.GetInstance<ILabel>();
            label3.Text = "Label 33333333333 BOLD";
            label3.FontFamily = "Arial";
            label3.FontAttribute = FontAttribute.Bold;
            
            var label4 = di.GetInstance<ILabel>();
            label4.Text = "Label 33333333333 Italic";
            //label4.FontFamily = "Baghdad";
            label4.FontAttribute = FontAttribute.Italic;
            
            var label5 = di.GetInstance<ILabel>();
            label5.Text = "Label Reg";
            //label5.FontFamily = "Herculanum";
            label5.FontAttribute = FontAttribute.Regular;

            stackLayout3.Add(label);
            stackLayout3.Add(label2);
            stackLayout3.Add(label3);
            stackLayout3.Add(label4);
            stackLayout3.Add(label5);
            
            stackLayout.Add(stackLayout2);
            stackLayout.Add(stackLayout3);

            Content = stackLayout as View;
        }
    }
}