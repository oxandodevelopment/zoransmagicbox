using ZoransMagicBox.Core.DI;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.DI
{
    public class ElementContainer : IElementContainer
    {
        private readonly IContainer _container;

        public ElementContainer(IContainer container)
        {
            _container = container;
        }

        public T GetInstance<T>() where T : class, IElement
        {
            return _container.GetInstance<T>();
        }

        public void Register<T, TY>() where TY : class, T, IElement where T : class
        {
            _container.Register<T, TY>(InstanceType.Instance);
        }
    }
}