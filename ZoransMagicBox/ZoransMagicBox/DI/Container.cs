using SimpleInjector;
using ZoransMagicBox.Core.DI;

namespace ZoransMagicBox.DI
{
    public class Container : IContainer
    {
        private readonly SimpleInjector.Container _container;

        public Container(SimpleInjector.Container container)
        {
            _container = container;
        }
        
        public T GetInstance<T>() where T : class
        {
            return _container.GetInstance<T>();
        }

        public void Register<T, TY>(InstanceType instanceType) where TY : class, T where T : class
        {
            var lifeStyle = instanceType == InstanceType.Instance
                ? Lifestyle.Transient
                : Lifestyle.Singleton;
            
            _container.Register<T, TY>(lifeStyle);
        }
    }
}