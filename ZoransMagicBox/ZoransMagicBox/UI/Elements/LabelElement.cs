using System;
using Xamarin.Forms;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.UI.Elements
{
    public class LabelElement : Label, ILabel
    {
        public new double Width
        {
            get => base.Width;
            set => WidthRequest = value;
        }

        public new double Height
        {
            get => base.Height;
            set => HeightRequest = value;
        }

        public IElementModel BindingModel
        {
            get => (IElementModel)BindingContext;
            set => BindingContext = value;
        }

        public FontAttribute FontAttribute
        {
            get
            {
                switch (FontAttributes)
                {
                    case FontAttributes.Bold: return FontAttribute.Bold;
                    case FontAttributes.Italic: return FontAttribute.Italic;
                    case FontAttributes.None: return FontAttribute.Regular;
                    default: throw new ArgumentOutOfRangeException();
                }
            }
            set
            {
                switch (value)
                {
                    case FontAttribute.Regular: FontAttributes = FontAttributes.None; break;
                    case FontAttribute.Bold: FontAttributes = FontAttributes.Bold; break;
                    case FontAttribute.Italic: FontAttributes = FontAttributes.Italic; break;
                    default: throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            } 
        }
    }
}