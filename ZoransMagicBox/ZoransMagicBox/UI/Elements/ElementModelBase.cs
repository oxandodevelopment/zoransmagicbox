using System.ComponentModel;
using System.Runtime.CompilerServices;
using ZoransMagicBox.Annotations;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.UI.Elements
{
    public abstract class ElementModelBase : IElementModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}