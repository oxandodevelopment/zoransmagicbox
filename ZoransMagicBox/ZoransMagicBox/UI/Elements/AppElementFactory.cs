using Xamarin.Forms;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.UI.Elements
{
    public class AppElementFactory : IAppElementFactory
    {
        public IMainNavItem CreateMainNavItem(IMainNavItemConfig config)
        {
            throw new System.NotImplementedException();
        }

        public IButton CreateButton(IButtonConfig config)
        {
            throw new System.NotImplementedException();
        }

        public IButton CreateLabel(ILabelConfig config)
        {
            throw new System.NotImplementedException();
        }
    }
}