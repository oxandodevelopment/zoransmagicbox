using System;
using Xamarin.Forms;
using ZoransMagicBox.Core.DI;
using ZoransMagicBox.Core.UI.Elements;
using Color = System.Drawing.Color;

namespace ZoransMagicBox.UI.Elements
{
    public partial class MainNavItem : ContentElement, IMainNavItem, IOnInit
    {
        private readonly IElementContainer _elementContainer;
        
        public static readonly BindableProperty TextProperty = BindablePropertyCreator<MainNavItem, string>(
            nameof(Text),
            (declarer, oldValue, newValue) =>
            {
                declarer._label.Text = newValue;
            }
        );
        public string Text { get => (string) GetValue(TextProperty); set => SetValue(TextProperty, value); }
        
        public static readonly BindableProperty IconProperty = BindablePropertyCreator<MainNavItem, string>(
            nameof(Icon),
            (declarer, oldValue, newValue) =>
            {
                declarer._iconLabel.Text = newValue;
            }
        );
        public string Icon
        {
            get => (string) GetValue(IconProperty);
            set => SetValue(IconProperty, value);
        }

        private void OnIconChange(string oldValue, string newValue)
        {
            
        }
        
        public string IconColor { get; set; }
        public bool IsSelected { get; set; }
        public Color SelectedBackgroundColor { get; set; }
        public bool IsHovering { get; set; }
        public bool HoveringBackgroundColor { get; set; }

        private IStackElement _stack;
        private ILabel _iconLabel;
        private ILabel _label;

        public MainNavItem(IElementContainer elementContainer)
        {
            _elementContainer = elementContainer;
        }
        
        public void OnInit()
        {
            _stack = _elementContainer.GetInstance<IStackElement>();
            _label = _elementContainer.GetInstance<ILabel>();
            _iconLabel = _elementContainer.GetInstance<ILabel>();
            
            _stack.Add(_iconLabel);
            _stack.Add(_label);
            
            Content = _stack as View;
        }
    }
}
