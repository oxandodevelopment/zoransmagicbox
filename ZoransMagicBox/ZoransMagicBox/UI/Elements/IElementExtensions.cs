using System;
using Xamarin.Forms;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.UI.Elements
{
    public static class IElementExtensions
    {
        public static BindableProperty BindablePropertyCreator<TDeclarationType, TValueType>(
            this TDeclarationType element,
            string propertyName, 
            Action<TDeclarationType, TValueType, TValueType> onPropertyChanged = null,
            Func<TDeclarationType, TValueType, bool> onValidate = null
        ) where TDeclarationType : BindableObject, IElement
        {
            return BindableProperty.Create(
                propertyName,
                typeof(TValueType),
                typeof(TDeclarationType),
                default(TValueType),
                BindingMode.Default,
                (bindable, value) =>
                {
                    if (onValidate == null) return true;
                    
                    var declarer = (TDeclarationType) bindable;
                    var oldVal = (TValueType) value;

                    return onValidate.Invoke(declarer, oldVal);
                },
                (bindable, value, newValue) =>
                {
                    if (onPropertyChanged == null) return;

                    var declarer = (TDeclarationType)bindable;
                    var oldVal = (TValueType) value;
                    var newVal = (TValueType) newValue;
                    
                    onPropertyChanged.Invoke(declarer, oldVal, newVal);
                });
        }
    }
}