using Xamarin.Forms;
using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.UI.Elements
{
    public class StackElement : StackLayout, IStackElement
    {
        public new double Width
        {
            get => base.Width;
            set => base.WidthRequest = value;
        }

        public new double Height
        {
            get => base.Height;
            set => base.HeightRequest = value;
        }
        public IElementModel BindingModel
        {
            get => (IElementModel) BindingContext;
            set => BindingContext = value;
        }

        public new Orientation Orientation
        {
            get => base.Orientation == StackOrientation.Horizontal ? Orientation.Horizontal : Orientation.Vertical; 
            set => base.Orientation = value == Orientation.Horizontal ? StackOrientation.Horizontal : StackOrientation.Vertical;
        }
        
        public void Add(IElement element)
        {
            Children.Add((View)element);
        }
    }
}