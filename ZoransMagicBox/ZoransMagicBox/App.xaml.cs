﻿using System;
using SimpleInjector;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZoransMagicBox.Core.DI;
using ZoransMagicBox.Core.UI.Elements;
using ZoransMagicBox.DI;
using ZoransMagicBox.UI.Elements;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace ZoransMagicBox
{
    public partial class App : Application
    {
        public static SimpleInjector.Container Container;
        
        public App()
        {
            InitializeComponent();
            
            // 1. Create a new Simple Injector container
            Container = new SimpleInjector.Container();
            var container = new DI.Container(Container);
            var elementContainer = new ElementContainer(container);
            elementContainer.Register<ILabel, LabelElement>();
            elementContainer.Register<IStackElement, StackElement>();

            // 2. Configure the container (register)
            Container.Register<IContainer>(() => container, Lifestyle.Singleton);
            Container.Register<IElementContainer>(() => elementContainer, Lifestyle.Singleton);
            
            // 3. Verify your configuration
            Container.Verify();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}