using System.ComponentModel;
using System.Drawing;

namespace ZoransMagicBox.Core.UI.Elements
{
    public interface IAppElementFactory
    {
        IMainNavItem CreateMainNavItem(IMainNavItemConfig config);
        IButton CreateButton(IButtonConfig config);
        IButton CreateLabel(ILabelConfig config);
    }
    
    public interface IOxandoAppElementFactory
    {
        IElement CreateApplicationButton();
        IElement CreateApplicationTitle();
        IElement CreateApplicationMenu();
        IElement CreateApplicationMenuButton();
        IElement CreateSystemMenu();
        IElement CreateSystemMenuButton();
        IElement CreateHomePageButton();
        IElement CreateAdvancedSearchButton();
        IElement CreatePageLeftMenuActionButton();
        IElement CreatePageLeftMenuEntry();
        IElement CreatePageRightMenuActionButton();
        IElement CreatePageRightMenuEntry();
    }

    public interface IElementFactory
    {
        ILabel CreateLabel();
        ILabel CreateIcon();
        IStackElement CreateStack();
        IElement CreateScrollableArea();
        IElement CreateList();
        IElement CreateTextField();
        IElement CreateTextArea();
        IElement CreateIconButton();
    }

    public interface IElement
    {
        double Width { get; set; }
        double Height { get; set; }
        public IElementModel BindingModel { get; set; }
    }

    public enum Orientation
    {
        Horizontal,
        Vertical
    }
    
    public interface IStackElement : IElement
    {
        Orientation Orientation { get; set; }
        void Add(IElement element);
    }
    
    public interface IMainNavItem : IElement
    {
        string Text { get; set; }
        string Icon { get; set; }
        string IconColor { get; set; }

        bool IsSelected { get; set; }
        Color SelectedBackgroundColor { get; set; }
        
        bool IsHovering { get; set; }
        bool HoveringBackgroundColor { get; set; }
    }
    
    public interface IMainNavIteModel : INotifyPropertyChanged
    {
        string Icon { get; set; }
        string Text { get; set; }
    }

    public interface IElementModel : INotifyPropertyChanged
    {
        
    }
    
    public interface IElementConfig : INotifyPropertyChanged
    {
        public IElementModel Model { get; }
    }

    public interface IMainNavItemConfig : IElementConfig
    {
        IMainNavIteModel Model { get; }
    }
    
    public interface IButton : IElement
    {
        
    }
    
    public interface IButtonConfig : IElementConfig
    {
        
    }
    
    public interface ILabel : IElement
    {
        string Text { get; set; }
        string FontFamily { get; set; }
        FontAttribute FontAttribute { get; set; }
    }
    
    public enum FontAttribute
    {
        Regular,
        Bold,
        Italic
    }
    
    public interface ILabelConfig : IElementConfig
    {
        
    }

    public interface IOnInit
    {
        void OnInit();
    }
}