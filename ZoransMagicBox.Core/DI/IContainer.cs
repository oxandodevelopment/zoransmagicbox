using ZoransMagicBox.Core.UI.Elements;

namespace ZoransMagicBox.Core.DI
{
    public interface IElementContainer
    {
        T GetInstance<T>() where T : class, IElement;
        void Register<T, TY>() where TY : class, T, IElement where T : class;
    }
    
    public interface IContainer
    {
        T GetInstance<T>() where T : class;
        void Register<T, TY>(InstanceType instanceType) where TY : class, T where T : class;
    }

    public enum InstanceType
    {
        Instance,
        Singleton
    }
}