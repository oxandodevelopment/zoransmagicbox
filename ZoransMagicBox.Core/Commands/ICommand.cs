using System;
using System.Threading.Tasks;

namespace ZoransMagicBox.Core.Commands
{
    public interface IApplicationCommandParameter
    {
        
    }
    
    public interface IPageCommandParameter
    {
        
    }
    
    public interface IElementCommandParameter
    {
        
    }
    
    public interface ICommandRequestResult
    {
        
    }
    
    public interface ICommandRequestHandler<T>
    {
        Task Request<TY>(TY parameter) where TY : T;
    }

    public interface IApplicationCommandRequestHandler : ICommandRequestHandler<IApplicationCommandParameter>
    {
        
    }
}
